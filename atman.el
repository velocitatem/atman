;;; atman.el --- A package to help you manage your focus and state of mind -*- lexical-binding: t -*-

;; Author: Daniel Rosel
;; Maintainer: Daniel Rosel
;; Version: 0.1
;; Package-Requires: (dependencies)
;; Homepage: gitlab.com/velocitatem/atman
;; Keywords: focus, time, manage, attention, ADD, ADHD, self, mind, mental snapshots, assistant


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:


(defcustom atman/snapshots '("School" "Work" "Fun" "Personal" "Rest") "A list of different snapshots used in atman")


(defun atman/load-phase-alpha (selected-snapshot)
  (interactive)
  (setq atman/spinning-snapshot selected-snapshot)
  ;; playing mp3 asynchronously
  (atman/play-mp3 (concat "/home/velocitatem/.atman/" selected-snapshot "/audio/alpha.mp3"))
  ;; run the alpha.sh script in the selected-snapshot directory
  (shell-command (concat "/home/velocitatem/.atman/" selected-snapshot "/alpha.sh"))
  ;; load the alpha.el file in the selected-snapshot directory if the file exists
  (if (file-exists-p (concat "/home/velocitatem/.atman/" selected-snapshot "/alpha.el"))
      (load-file (concat "/home/velocitatem/.atman/" selected-snapshot "/alpha.el"))
      ;; run the atma/custom/alpha function in the alpha.el file
      (atma/custom/alpha)
    )
)


(defun atman/load-phase-lambda ()
  (interactive)
  ;; playing mp3 asynchronously
  (atman/play-mp3 (concat "/home/velocitatem/.atman/" atman/spinning-snapshot "/audio/lambda.mp3"))
  ;; run the lambda.sh script in the selected-snapshot directory
  (shell-command (concat "/home/velocitatem/.atman/" atman/spinning-snapshot "/lambda.sh"))
  ;; load the lambda.el file in the selected-snapshot directory if the file exists
  (if (file-exists-p (concat "/home/velocitatem/.atman/" atman/spinning-snapshot "/lambda.el"))
      (load-file (concat "/home/velocitatem/.atman/" atman/spinning-snapshot "/lambda.el"))
      ;; run the atma/custom/lambda function in the lambda.el file
      (atma/custom/lambda)
    )
)

(defun atman/play-mp3 (file)
  "Play an mp3 file in the background"
  (start-process "mpg123" "Alpha Music" "mpg123" file)
)

;; (atman/play-mp3 "/home/velocitatem/.atman/School/audio/alpha.mp3")

(defun atman/begin ()
  "function to start up a snapshot"
  (interactive)
  (let ((selected-snapshot (completing-read "Select snapshot: " atman/snapshots)))
    (message selected-snapshot)
    (message (concat "/home/velocitatem/.atman/" selected-snapshot "/audio/alpha.mp3"))
    (atman/load-phase-alpha selected-snapshot)
    )
)

(defun atman/stop ()
  "function to spin-down the current snapshot"
  (interactive)

  )


(provide 'atman)

;;; atman.el ends here
