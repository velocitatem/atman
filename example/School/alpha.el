(defun atman/custom/alpha ()
  "Customized startup script for the current snapshot"
  (interactive)
  ;; ask the user if they want to start the org-pomodor timer
  (when (y-or-n-p "Start pomodoro timer?")
    ;; create org-mode buffer with a heading called "School Work"
    (find-file "~/.atman/School/org.org")
    (goto-char (point-max))
    ;; create a new heading with the current date and time
    ;; concat the date and time into a string
    (let ((date-time (concat (format-time-string "%Y-%m-%d %H:%M"))))
      ;; insert the date-time string into the buffer
      (insert (concat "* School Work - " date-time ))
      (org-pomodoro)
      )
  )
  ;; ask the user if they want to open the lectorg hub
  (when (y-or-n-p "Open lectorg hub?")
    (lectorg/class-hub-open)
  )
)
