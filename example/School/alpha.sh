#!/usr/bin/env bash
for file in $(ls ./images/a.* | shuf | head -n 4)
do
    feh -F --reload 1 $file;
done
