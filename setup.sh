#!/usr/bin/env bash
mkdir ~/.atman
# create an atman directory in usr share
sudo mkdir /usr/share/atman
# copy the defaults directory into the usr share atman directory
sudo cp -r defaults /usr/share/atman
# copy the create snapshot script into .atman
cp create-snapshot.sh ~/.atman
