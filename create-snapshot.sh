#!/usr/bin/env bash
echo "Enter Snapshot Name:"
read NAME
PATH_TO_DIR="~/.atman/"NAME
# create a directory for the snapshot
mkdir $PATH_TO_DIR
# create image directory in the snapshot
mkdir $PATH_TO_DIR"/images"
# copy the default alpha.sh file from usr/share to the snapshot
cp /usr/share/atman/defaults/alpha.sh $PATH_TO_DIR
# ask user if they want to create customized scripts in the snapshot
echo "Would you like to create customized elisp scripts for this snapshot? (y/n)"
read CUSTOM
# if yes, then create the files alpha.el and lambda.el
if [ $CUSTOM = "y" ]; then
    touch $PATH_TO_DIR"/alpha.el"
    touch $PATH_TO_DIR"/lambda.el"
fi
